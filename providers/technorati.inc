<?php

$provider['technorati'] = array(
  'name' => 'Technorati',
  'create_url' => 'technorati_create_url',
  'source' => 'Technorati',
  'type' => 'Blogs',
);

function technorati_create_url($topic) {
  return sprintf('http://feeds.technorati.com/search/"%s"', $topic);
}


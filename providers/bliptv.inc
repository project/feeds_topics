<?php

$provider['bliptv'] = array(
  'name' => 'blip.tv',
  'create_url' => 'bliptv_create_url',
  'source' => 'blip.tv',
  'type' => 'Videos',
);

function bliptv_create_url($topic) {
  return sprintf('http://blip.tv/?y=0;search="%s";page=1;x=0;s=search&skin=rss', $topic);
}


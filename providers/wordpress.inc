<?php

$provider['wordpress'] = array(
  'name' => 'WordPress',
  'create_url' => 'wordpress_create_url',
  'source' => 'WordPress',
  'type' => 'Blogs',
);

function wordpress_create_url($topic) {
  return sprintf('http://en.search.wordpress.com/?q="%s"&s=date&f=feed', $topic);
}


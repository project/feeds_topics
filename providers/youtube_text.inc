<?php

$provider['youtube_text'] = array(
  'name' => 'YouTube',
  'create_url' => 'youtube_text_create_url',
  'source' => 'YouTube',
  'type' => 'Videos',
);

function youtube_text_create_url($topic) {
  return sprintf('http://gdata.youtube.com/feeds/base/videos?q="%s"&client=ytapi-youtube-search&v=2', $topic);
}


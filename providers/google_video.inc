<?php

$provider['google_video'] = array(
  'name' => 'Google Video',
  'create_url' => 'google_video_create_url',
  'source' => 'Google',
  'type' => 'Videos',
);

function google_video_create_url($topic) {
  return sprintf('http://video.google.com/videosearch?q="%s"&output=rss', $topic);
}


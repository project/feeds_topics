<?php

$provider['icerocket'] = array(
  'name' => 'IceRocket',
  'create_url' => 'icerocket_create_url',
  'source' => 'IceRocket',
  'type' => 'Blogs',
);

function icerocket_create_url($topic) {
  return sprintf('http://blogs.icerocket.com/search?q="%s"&rss=1', $topic);
}


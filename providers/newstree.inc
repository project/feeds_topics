<?php

$provider['newstree'] = array(
  'name' => 'NewsTree',
  'create_url' => 'newstree_create_url',
  'source' => 'NewsTree',
  'type' => 'News',
);

function newstree_create_url($topic) {
  return sprintf('http://newstree.org/opensearch?query="%s"&hs=0&hp=10', $topic);
}


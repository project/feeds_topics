<?php

$provider['dailymotion'] = array(
  'name' => 'Dailymotion',
  'create_url' => 'dailymotion_create_url',
  'source' => 'Dailymotion',
  'type' => 'Videos',
);

function dailymotion_create_url($topic) {
  return sprintf('http://www.dailymotion.com/rss/relevance/search/"%s"/1', $topic);
}


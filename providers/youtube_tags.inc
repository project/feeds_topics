<?php

$provider['youtube_tags'] = array(
  'name' => 'YouTube (tags)',
  'create_url' => 'youtube_tags_create_url',
  'source' => 'YouTube',
  'type' => 'Videos',
);

function youtube_tags_create_url($topic) {
  return sprintf('http://gdata.youtube.com/feeds/base/videos/-/%s?client=ytapi-youtube-browse&v=2', $topic);
}


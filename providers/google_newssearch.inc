<?php

$provider['google_newssearch'] = array(
  'name' => 'Google News Search',
  'create_url' => 'google_newssearch_create_url',
  'source' => 'Google',
  'type' => 'News',
);

function google_newssearch_create_url($topic) {
  return sprintf('http://news.google.com/news?hl=en&ned=us&ie=UTF-8&q="%s"&nolr=1&output=rss', $topic);
}


<?php

$provider['google_blogsearch'] = array(
  'name' => 'Google Blog Search',
  'create_url' => 'google_blogsearch_create_url',
  'source' => 'Google',
  'type' => 'Blogs',
);

function google_blogsearch_create_url($topic) {
  return sprintf('http://blogsearch.google.com/blogsearch_feeds?q="%s"&ie=utf-8&num=10&output=atom', $topic);
}


<?php

$provider['twitter_hashtags'] = array(
  'name' => 'Twitter (hashtags)',
  'create_url' => 'twitter_hashtags_create_url',
  'source' => 'Twitter',
  'type' => 'Microblogs',
);

function twitter_hashtags_create_url($topic) {
  return sprintf('http://search.twitter.com/search.atom?q=+%s', $topic);
}


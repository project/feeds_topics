<?php

$provider['truveo'] = array(
  'name' => 'Truveo',
  'create_url' => 'truveo_create_url',
  'source' => 'Truveo',
  'type' => 'Videos',
);

function truveo_create_url($topic) {
  return sprintf('http://xml.truveo.com/rss?query="%s" sort:mostRecent', $topic);
}


<?php

$provider['ask_bloglines'] = array(
  'name' => 'Ask/Bloglines',
  'create_url' => 'ask_bloglines_create_url',
  'source' => 'Bloglines',
  'type' => 'Blogs',
);

function ask_bloglines_create_url($topic) {
  return sprintf('http://www.bloglines.com/search?q="%s"&format=rss', $topic);
}


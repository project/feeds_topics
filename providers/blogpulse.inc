<?php

$provider['blogpulse'] = array(
  'name' => 'BlogPulse',
  'create_url' => 'blogpulse_create_url',
  'source' => 'BlogPulse',
  'type' => 'Blogs',
);

function blogpulse_create_url($topic) {
  return sprintf('http://www.blogpulse.com/rss?query="%s"', $topic);
}


<?php

$provider['oneriot'] = array(
  'name' => 'OneRiot',
  'create_url' => 'oneriot_create_url',
  'source' => 'OneRiot',
  'type' => 'Blogs',
);

function oneriot_create_url($topic) {
  return sprintf('http://www.oneriot.com/rss/serp?q="%s"', $topic);
}


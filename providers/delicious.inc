<?php

$provider['delicious'] = array(
  'name' => 'Delicious',
  'create_url' => 'delicious_create_url',
  'source' => 'Delicious',
  'type' => 'Bookmarks',
);

function delicious_create_url($topic) {
  return sprintf('http://delicious.com/tag/%s', $topic);
}


<?php

$provider['twitter_statuses'] = array(
  'name' => 'Twitter',
  'create_url' => 'twitter_statuses_create_url',
  'source' => 'Twitter',
  'type' => 'Microblogs',
);

function twitter_statuses_create_url($topic) {
  return sprintf('http://search.twitter.com/search.atom?q="%s"', $topic);
}


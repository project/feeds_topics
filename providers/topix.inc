<?php

$provider['topix'] = array(
  'name' => 'Topix',
  'create_url' => 'topix_create_url',
  'source' => 'Topix',
  'type' => 'News',
);

function topix_create_url($topic) {
  return sprintf('http://www.topix.com/search/article?blogs=1&dedup=1&xml=1&q="%s"', $topic);
}


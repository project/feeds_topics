<?php

$provider['flickr'] = array(
  'name' => 'Flickr',
  'create_url' => 'flickr_create_url',
  'source' => 'Flickr',
  'type' => 'Photos',
);

function flickr_create_url($topic) {
  return sprintf('http://api.flickr.com/services/feeds/photos_public.gne?tags="%s"&format=rss_200', $topic);
}


<?php

class FeedsTopicParser extends FeedsParser {

  public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $topics = $source_config['topics'];
    $feeds = array();
    $providers = module_invoke_all('feeds_topics_providers');
    if (!empty($this->config['providers_expose']) && isset($source_config['advanced']['providers'])) {
      $providers = array_intersect_key($providers, array_filter($source_config['advanced']['providers']));
    }
    else if (!empty($this->config['providers'])) {
      $providers = array_intersect_key($providers, array_filter($this->config['providers']));
    }
    foreach (preg_split("/(\r?\n)/", $topics) as $topic) {
      $topic = trim($topic);
      if (!empty($topic)) foreach ($providers as $provider) {
        $provider['topic'] = $topic;
        $feeds[] = array(
          'url' => call_user_func($provider['create_url'], urlencode($topic)),
          'source' => $provider['source'],
          'type' => $provider['type'],
          'name' => $provider['name'],
          'topic' => $topic,
          'title' => module_exists('token') && !empty($this->config['title_pattern']) ? 
            token_replace_multiple($this->config['title_pattern'], array('global' => NULL, 'topic_provider' => $provider)) : 
            t('!name for topic "!topic"', array('!name' => $provider['name'], '!topic' => $topic)),
        );
      }
    }
    $batch->setItems($feeds);
  }

  public function getMappingSources() {
    return array(
      'title' => t('Feed title'),
      'url' => t('Feed URL'),
      'type' => t('Feed type'),
      'name' => t('Feed name'),
      'topic' => t('Topic keywords'),
      'source' => t('Source name'),
    );
  }

  public function sourceForm($source_config) {
    $form = array();
    $form['topics'] = array(
      '#type' => 'textarea',
      '#title' => t('Topic keywords'),
      '#description' => t('Enter your keywords of interest, one phrase per line.'),
      '#default_value' => @$source_config['topics'],
    );
    if ($this->config['providers_expose']) {
      $form['advanced'] = array(
        '#type' => 'fieldset',
        '#title' => t('Advanced'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $providers = array();
      foreach (module_invoke_all('feeds_topics_providers') as $key => $provider) {
        $providers[$key] = $provider['name'];
      }
      $providers = array_intersect_key($providers, array_filter($this->config['providers']));
      ksort($providers);
      $form['advanced']['providers'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Sources'),
        '#options' => $providers,
        '#default_value' => !empty($source_config['advanced']['providers']) ? $source_config['advanced']['providers'] : array_combine(array_keys($providers), array_keys($providers)),
      );
    }
    return $form;
  }

  public function sourceFormValidate(&$source_config) {
    $topics = trim($source_config['topics']);
    if (empty($topics)) {
      form_set_error('feeds][topics', t('Please enter one or more phrases to monitor.'));
    }
    if ($this->config['providers_expose']) {
      $providers = array_filter($source_config['advanced']['providers']);
      if (empty($providers)) {
        form_set_error('feeds][advanced][providers]', t('Please select one or more sources.'));
      }
    }
  }

  public function configDefaults() {
    return array(
      'title_pattern' => '',
      'providers' => FALSE,
      'providers_expose' => FALSE,
    );
  }

  public function configForm($form_state) {
    $form = array();
    if (module_exists('token')) {
      $form['title_pattern'] = array(
        '#type' => 'textfield',
        '#title' => t('Title pattern'),
        '#description' => t('Enter the pattern for the feed title using the tokens below. Leave blank for default title. Available tokens:') . theme('fieldset', array(
          '#title' => t('Available tokens'),
          '#value' => theme('token_help', array('global', 'topic_provider')),
          '#collapsed' => TRUE,
          '#collapsible' => TRUE,
        )),
        '#default_value' => $this->config['title_pattern'],
      );
    }

    $providers = array();
    foreach (module_invoke_all('feeds_topics_providers') as $key => $provider) {
      $providers[$key] = $provider['name'];
    }
    ksort($providers);
    $form['providers'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Providers'),
      '#options' => $providers,
      '#default_value' => $this->config['providers'] === FALSE ? array_combine(array_keys($providers), array_keys($providers)) : $this->config['providers'],
      '#description' => t('Select the topic providers you would like to include in this importer.'),
    );
    $form['providers_expose'] = array(
      '#type' => 'checkbox',
      '#title' => t('Expose providers list in source form.'),
      '#default_value' => $this->config['providers_expose'],
    );

    return $form;
  }
}

